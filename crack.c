#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN = 20;        // Maximum any password will be
const int HASH_LEN = 33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char * targetHash, char * dictionaryFilename)
{
    // Open the dictionary file
    FILE *pws = fopen(dictionaryFilename, "r");

    // Loop through the dictionary file, one line
    // at a time.
    char word[PASS_LEN];
    char *password;
    while(fgets(word, PASS_LEN, pws) != NULL)
    {
        //trim newline?
        char *nl = strchr(word, '\n');
        if (nl) *nl = '\0';

        // Hash each password. Compare to the target hash.
        // If they match, return the corresponding password.
        char *hash = md5(word, strlen(word));
        if (strcmp(hash, targetHash) == 0)
        {
            password = word;
            free(hash);
            //ERROR FIXED
            //close pws here since it will be reopened with every function call
            fclose(pws);
            return password;
        }
        //Free up memory
        free(hash);

    }

    // Close the dictionary file.
    fclose(pws);

    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        fprintf(stderr, "Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *f = fopen(argv[1], "r");
    if (!f)
    {
        fprintf(stderr, "Can't open %s for reading: ", argv[1]);
        perror(NULL);
        exit(1);
    }

    // For each hash, crack it by passing it to crackHash
    char targetHash[34];
    char *password;
    while (fgets(targetHash, 34, f) != NULL)
    {
        //trim newline
        char *nl = strchr(targetHash, '\n');
        if (nl) *nl = '\0';

        password = crackHash(targetHash, argv[2]);

        // Display the hash along with the cracked password:
        // 5d41402abc4b2a76b9719d911017c592 hello
        printf("%s %s\n", targetHash, password);
    }
    
    // Close the hash file
    fclose(f);
    

    //to compile: clang crack.c md5.c -l crypto
    //to run: ./a.out hashes.txt rockyou100.txt
    
}
